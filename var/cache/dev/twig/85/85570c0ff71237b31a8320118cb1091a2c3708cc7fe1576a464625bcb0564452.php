<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/pagecv.html.twig */
class __TwigTemplate_a3f240bbc5ab39aa6a0d7503de3df8eb083918ad5a77a8040673f2b024fc043a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/pagecv.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/pagecv.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/pagecv.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<head>

</head>
<div class=\"page\">
     <div class=\"infophoto\">
          <div class= \"info\">
          <p><strong> Soraya TOUMI</strong>
          <p> 570 route de Ganges, 34070 Montpellier<br/>
          <p>fetimatoumi@yahoo.com</p>
       
       </div>
               
      </div>
       <div class= \"section\">
            <h2>formation</h2>
            <div class= \"sec-gauche\" id=\"forma-gauche\">
           <p> 2019 </p>
           <p> 2018 <br/></p>
           <p> 2017 <br/></p>

           </div>
            <div class =\"sec-droite\">
             <p><span class= \"titre\">  Université de Montpellier</span><br/> Master II Inforormatique</p>
             <p><span class= \"titre\">   Université de Montpellier </span><br/>Master II Inforormatique</p>
             <p><span class= \"titre\"> Université de Toulouse 3 </span><br/> épidémiologie vétérinaire</p>

       </div>
      </div>
        <div class= \"section\">
                  <h2>Experiences Professionnelles</h2>
                  <div class= \"sec-gauche\" id=\"forma-gauche\">
                 <p> 2019 </p>
                 <p> 2019 <br/></p>
                 <p> 2019  <br/></p>
                 <p> 2019 <br/></p>

                 </div>
                  <div class =\"sec-droite\">
                   <div class= \"lien\"><a href=\"https://github.com/solinatoumi/symfoTest\" class=\"btn btn-primary type =\"submit\">guithub</a></div>
                   <p><span class= \"titre\"> création d'un cv avec le framework symfony </span><br/> Avec le framework php :symfonie,Doctrine,Twig</p>
                   <div class= \"lien\"><a href=\"https://github.com/solinatoumi/responsiveskeleton\" class=\"btn btn-primary type =\"submit\"> guithub </a></div>
                   <p><span class= \"titre\"> gestion des templates avec symfony </span><br/> d'affichage avec le framework symfony</p>
                   <div class= \"lien\"><a href=\"https://github.com/solinatoumi/generateur-de-sites-web\" class=\"btn btn-primary type =\"submit\"> guithub  </a></div>
                   <p><span class= \"titre\"> Création d’un générateur de site web </span><br/> en utlisant D3 et OpenLayer</p>
                   

             </div>
            </div>
            
                   <div class= \"section\">
                              <h2>Centeres interets </h2>
                              <div class= \"sec-gauche\">
                             <p> Randonnées </p>
                             <p> Voyages<br/></p>
                             <p>Cuisine <br/></p>


                             </div>
                              <div class =\"sec-droite\">
                               <p><span class> nature</span><br/></p>
                               <p><span class> Allemagne,Italy</span><br/></p>
                               <p><span class> Algerienne</span><br/></p>


                         </div>
                        </div>

      </div>





";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/pagecv.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 5,  77 => 4,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig'%}
{% block title %}
{% endblock %}
{% block body %}
<head>

</head>
<div class=\"page\">
     <div class=\"infophoto\">
          <div class= \"info\">
          <p><strong> Soraya TOUMI</strong>
          <p> 570 route de Ganges, 34070 Montpellier<br/>
          <p>fetimatoumi@yahoo.com</p>
       
       </div>
               
      </div>
       <div class= \"section\">
            <h2>formation</h2>
            <div class= \"sec-gauche\" id=\"forma-gauche\">
           <p> 2019 </p>
           <p> 2018 <br/></p>
           <p> 2017 <br/></p>

           </div>
            <div class =\"sec-droite\">
             <p><span class= \"titre\">  Université de Montpellier</span><br/> Master II Inforormatique</p>
             <p><span class= \"titre\">   Université de Montpellier </span><br/>Master II Inforormatique</p>
             <p><span class= \"titre\"> Université de Toulouse 3 </span><br/> épidémiologie vétérinaire</p>

       </div>
      </div>
        <div class= \"section\">
                  <h2>Experiences Professionnelles</h2>
                  <div class= \"sec-gauche\" id=\"forma-gauche\">
                 <p> 2019 </p>
                 <p> 2019 <br/></p>
                 <p> 2019  <br/></p>
                 <p> 2019 <br/></p>

                 </div>
                  <div class =\"sec-droite\">
                   <div class= \"lien\"><a href=\"https://github.com/solinatoumi/symfoTest\" class=\"btn btn-primary type =\"submit\">guithub</a></div>
                   <p><span class= \"titre\"> création d'un cv avec le framework symfony </span><br/> Avec le framework php :symfonie,Doctrine,Twig</p>
                   <div class= \"lien\"><a href=\"https://github.com/solinatoumi/responsiveskeleton\" class=\"btn btn-primary type =\"submit\"> guithub </a></div>
                   <p><span class= \"titre\"> gestion des templates avec symfony </span><br/> d'affichage avec le framework symfony</p>
                   <div class= \"lien\"><a href=\"https://github.com/solinatoumi/generateur-de-sites-web\" class=\"btn btn-primary type =\"submit\"> guithub  </a></div>
                   <p><span class= \"titre\"> Création d’un générateur de site web </span><br/> en utlisant D3 et OpenLayer</p>
                   

             </div>
            </div>
            
                   <div class= \"section\">
                              <h2>Centeres interets </h2>
                              <div class= \"sec-gauche\">
                             <p> Randonnées </p>
                             <p> Voyages<br/></p>
                             <p>Cuisine <br/></p>


                             </div>
                              <div class =\"sec-droite\">
                               <p><span class> nature</span><br/></p>
                               <p><span class> Allemagne,Italy</span><br/></p>
                               <p><span class> Algerienne</span><br/></p>


                         </div>
                        </div>

      </div>





{% endblock %}
", "home/pagecv.html.twig", "/home/e20180010339/Téléchargements/projetsymfonySoraya/templates/home/pagecv.html.twig");
    }
}
