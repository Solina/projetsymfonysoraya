<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/pagepagepagepagepagecv.html.twig */
class __TwigTemplate_d84138e563a4b269159a01e8483c3744c08626e9bd3f4b83a3142213382bdeb0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/pagepagepagepagepagecv.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/pagepagepagepagepagecv.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/pagepagepagepagepagecv.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<head>

</head>
<div class=\"page\">
     <div class=\"infophoto\">
          <div class= \"info\">
          <p><strong> BEN AHMED Tassadit</strong> </br>Née le 17 Octobre 1994</p>
          <p> 64 Avenue de lodeve ,34070 Montpellier<br/>
          tassadit.benahmed@yahoo.fr</p>
       <p>Permis B (permis voiture)</p>
       </div>
                <div class= \"photo\">
               ";
        // line 18
        echo "
       </div>
      </div>
       <div class= \"section\">
            <h2>formation</h2>
            <div class= \"sec-gauche\" id=\"forma-gauche\">
           <p> 2019 </p>
           <p> 2018 <br/></p>
           <p> 2017 <br/></p>
           <p> 2016 <br/></p>

           </div>
            <div class =\"sec-droite\">
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier</span><br/> Master II Inforormatique</p>
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier </span><br/>Licence en Microbiologie</p>
             <p><span class= \"titre\"> Université de Mouloud Mammeri de Tizi-Ouzou,Algerie</span><br/>Licence en Microbiologie</p>
             <p><span class= \"titre\"> Lycee Ait Yahia,Algerie </span><br/> Bacalaureat Serie Sciences Experimentales</p>

       </div>
      </div>
        <div class= \"section\">
                  <h2>Experiences Professionnelles</h2>
                  <div class= \"sec-gauche\" id=\"forma-gauche\">
                 <p> 2019 </p>
                 <p> 2019 <br/></p>
                 <p> 2019  <br/></p>
                 <p> 2019 <br/></p>

                 </div>
                  <div class =\"sec-droite\">
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Projet site de présentation </span><br/> Avec le framework php :symfonie,Doctrine,Twig</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Gestion de Monuments </span><br/> Java ee avec son framework Spring, le moteur de template thymeleef,hibernate</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Fouille de Données (data maining avec weka)</span><br/>pretraitements avec des programmes python et java puis Wika</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Site Ecommerce Architecture MEAN </span><br/> Anguar,Node Js,MongoDB</p>

             </div>
            </div>
             <div class= \"section\">
                        <h2>formation</h2>
                        <div class= \"sec-gauche\" id=\"forma-gauche\">
                       <p> 2019 </p>
                       <p> 2018 <br/></p>
                       <p> 2017 <br/></p>
                       <p> 2016 <br/></p>

                       </div>
                        <div class =\"sec-droite\">
                         <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier</span><br/> Master II Inforormatique</p>
                         <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier </span><br/>Licence en Microbiologie</p>
                         <p><span class= \"titre\"> Université de Mouloud Mammeri de Tizi-Ouzou,Algerie</span><br/>Licence en Microbiologie</p>
                         <p><span class= \"titre\"> Lycee Ait Yahia,Algerie </span><br/> Bacalaureat Serie Sciences Experimentales</p>

                   </div>
                  </div>
                   <div class= \"section\">
                              <h2>Centeres interets </h2>
                              <div class= \"sec-gauche\">
                             <p> Musique</p>
                             <p> Voyages<br/></p>
                             <p>Cuisine <br/></p>


                             </div>
                              <div class =\"sec-droite\">
                               <p><span class> pratique la guitare</span><br/></p>
                               <p><span class> Tunisie,Maroc ,Espagne,Almagne..</span><br/></p>
                               <p><span class> Lol</span><br/></p>


                         </div>
                        </div>

      </div>





";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/pagepagepagepagepagecv.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 18,  87 => 5,  77 => 4,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig'%}
{% block title %}
{% endblock %}
{% block body %}
<head>

</head>
<div class=\"page\">
     <div class=\"infophoto\">
          <div class= \"info\">
          <p><strong> BEN AHMED Tassadit</strong> </br>Née le 17 Octobre 1994</p>
          <p> 64 Avenue de lodeve ,34070 Montpellier<br/>
          tassadit.benahmed@yahoo.fr</p>
       <p>Permis B (permis voiture)</p>
       </div>
                <div class= \"photo\">
               {# <img src=\"photo.jpg\" alt= \"tassadit\"/>#}

       </div>
      </div>
       <div class= \"section\">
            <h2>formation</h2>
            <div class= \"sec-gauche\" id=\"forma-gauche\">
           <p> 2019 </p>
           <p> 2018 <br/></p>
           <p> 2017 <br/></p>
           <p> 2016 <br/></p>

           </div>
            <div class =\"sec-droite\">
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier</span><br/> Master II Inforormatique</p>
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier </span><br/>Licence en Microbiologie</p>
             <p><span class= \"titre\"> Université de Mouloud Mammeri de Tizi-Ouzou,Algerie</span><br/>Licence en Microbiologie</p>
             <p><span class= \"titre\"> Lycee Ait Yahia,Algerie </span><br/> Bacalaureat Serie Sciences Experimentales</p>

       </div>
      </div>
        <div class= \"section\">
                  <h2>Experiences Professionnelles</h2>
                  <div class= \"sec-gauche\" id=\"forma-gauche\">
                 <p> 2019 </p>
                 <p> 2019 <br/></p>
                 <p> 2019  <br/></p>
                 <p> 2019 <br/></p>

                 </div>
                  <div class =\"sec-droite\">
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Projet site de présentation </span><br/> Avec le framework php :symfonie,Doctrine,Twig</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Gestion de Monuments </span><br/> Java ee avec son framework Spring, le moteur de template thymeleef,hibernate</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Fouille de Données (data maining avec weka)</span><br/>pretraitements avec des programmes python et java puis Wika</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/19941993/monprojetspringmvc\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Site Ecommerce Architecture MEAN </span><br/> Anguar,Node Js,MongoDB</p>

             </div>
            </div>
             <div class= \"section\">
                        <h2>formation</h2>
                        <div class= \"sec-gauche\" id=\"forma-gauche\">
                       <p> 2019 </p>
                       <p> 2018 <br/></p>
                       <p> 2017 <br/></p>
                       <p> 2016 <br/></p>

                       </div>
                        <div class =\"sec-droite\">
                         <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier</span><br/> Master II Inforormatique</p>
                         <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier </span><br/>Licence en Microbiologie</p>
                         <p><span class= \"titre\"> Université de Mouloud Mammeri de Tizi-Ouzou,Algerie</span><br/>Licence en Microbiologie</p>
                         <p><span class= \"titre\"> Lycee Ait Yahia,Algerie </span><br/> Bacalaureat Serie Sciences Experimentales</p>

                   </div>
                  </div>
                   <div class= \"section\">
                              <h2>Centeres interets </h2>
                              <div class= \"sec-gauche\">
                             <p> Musique</p>
                             <p> Voyages<br/></p>
                             <p>Cuisine <br/></p>


                             </div>
                              <div class =\"sec-droite\">
                               <p><span class> pratique la guitare</span><br/></p>
                               <p><span class> Tunisie,Maroc ,Espagne,Almagne..</span><br/></p>
                               <p><span class> Lol</span><br/></p>


                         </div>
                        </div>

      </div>





{% endblock %}
", "home/pagepagepagepagepagecv.html.twig", "C:\\Users\\tassa\\Desktop\\TP2symfo\\tp2\\templates\\home\\pagepagepagepagepagecv.html.twig");
    }
}
