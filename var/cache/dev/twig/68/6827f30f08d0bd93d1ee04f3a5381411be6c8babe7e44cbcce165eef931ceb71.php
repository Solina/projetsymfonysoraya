<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/cv.html.twig */
class __TwigTemplate_8d23660273bd3487d30845845cafda66880bd112cd20cc7ea59cfad5961b4efa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/cv.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/cv.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/cv.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<head>

</head>
<div class=\"page\">
     <div class=\"infophoto\">
          <div class= \"info\">
          <p><strong> SAADA Nassim</strong>
          <p> 6 Rue de l'Ecrin,34080 Montpellier<br/>
          saadanassim@yahoo.fr</p>
       <p>Permis Categorie B/p>
       </div>
                <div class= \"photo\">
               ";
        // line 18
        echo "
       </div>
      </div>
       <div class= \"section\">
            <h2>formation</h2>
            <div class= \"sec-gauche\" id=\"forma-gauche\">
           <p> 2019 </p>
           <p> 2018 <br/></p>
           <p> 2012 <br/></p>
           <p> 2007 <br/></p>

           </div>
            <div class =\"sec-droite\">
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier</span><br/> Master II Inforormatique</p>
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier </span><br/>DU Traitement de l'information & Intelegence Economique</p>
             <p><span class= \"titre\"> Université de Bejaia,Algerie</span><br/>Sciences de gestions Specialite finance</p>
             <p><span class= \"titre\"> Lycee mixte d'Amizour,Algerie </span><br/> Bacalaureat Serie Gestion et Economie</p>

       </div>
      </div>
        <div class= \"section\">
                  <h2>Experiences Professionnelles</h2>
                  <div class= \"sec-gauche\" id=\"forma-gauche\">
                 <p> 2019 </p>
                 <p> 2019 <br/></p>
                 <p> 2019  <br/></p>
                 <p> 2019 <br/></p>

                 </div>
                  <div class =\"sec-droite\">
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Projet site de présentation </span><br/> Avec le framework php :symfonie,Doctrine,Twig</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Gestion de Monuments </span><br/> Java ee avec son framework Spring, le moteur de template thymeleef,hibernate</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Fouille de Données (data maining avec weka)</span><br/>pretraitements avec des programmes python et java puis Wika</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Site Ecommerce Architecture MEAN </span><br/> Anguar,Node Js,MongoDB</p>

             </div>
            </div>
            
                   <div class= \"section\">
                              <h2>Centeres interets </h2>
                              <div class= \"sec-gauche\">
                             <p> Musique</p>
                             <p> Voyages<br/></p>
                             <p>Cuisine <br/></p>


                             </div>
                              <div class =\"sec-droite\">
                               <p><span class> pratique la guitare</span><br/></p>
                               <p><span class> Tunisie,Maroc ,Espagne,Almagne..</span><br/></p>
                               <p><span class> Lol</span><br/></p>


                         </div>
                        </div>

      </div>





";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/cv.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 18,  87 => 5,  77 => 4,  59 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig'%}
{% block title %}
{% endblock %}
{% block body %}
<head>

</head>
<div class=\"page\">
     <div class=\"infophoto\">
          <div class= \"info\">
          <p><strong> SAADA Nassim</strong>
          <p> 6 Rue de l'Ecrin,34080 Montpellier<br/>
          saadanassim@yahoo.fr</p>
       <p>Permis Categorie B/p>
       </div>
                <div class= \"photo\">
               {# <img src=\"photo.jpg\" alt= \"nassim\"/>#}

       </div>
      </div>
       <div class= \"section\">
            <h2>formation</h2>
            <div class= \"sec-gauche\" id=\"forma-gauche\">
           <p> 2019 </p>
           <p> 2018 <br/></p>
           <p> 2012 <br/></p>
           <p> 2007 <br/></p>

           </div>
            <div class =\"sec-droite\">
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier</span><br/> Master II Inforormatique</p>
             <p><span class= \"titre\"> Faculté des Sciences Université de Montpellier </span><br/>DU Traitement de l'information & Intelegence Economique</p>
             <p><span class= \"titre\"> Université de Bejaia,Algerie</span><br/>Sciences de gestions Specialite finance</p>
             <p><span class= \"titre\"> Lycee mixte d'Amizour,Algerie </span><br/> Bacalaureat Serie Gestion et Economie</p>

       </div>
      </div>
        <div class= \"section\">
                  <h2>Experiences Professionnelles</h2>
                  <div class= \"sec-gauche\" id=\"forma-gauche\">
                 <p> 2019 </p>
                 <p> 2019 <br/></p>
                 <p> 2019  <br/></p>
                 <p> 2019 <br/></p>

                 </div>
                  <div class =\"sec-droite\">
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Projet site de présentation </span><br/> Avec le framework php :symfonie,Doctrine,Twig</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Gestion de Monuments </span><br/> Java ee avec son framework Spring, le moteur de template thymeleef,hibernate</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Fouille de Données (data maining avec weka)</span><br/>pretraitements avec des programmes python et java puis Wika</p>
                   <div class= \"lien\"><a href=\"https://gitlab.com/NassimIPS/projetsymfony\" class=\"btn btn-primary type =\"submit\"> gitLab </a></div>
                   <p><span class= \"titre\"> Site Ecommerce Architecture MEAN </span><br/> Anguar,Node Js,MongoDB</p>

             </div>
            </div>
            
                   <div class= \"section\">
                              <h2>Centeres interets </h2>
                              <div class= \"sec-gauche\">
                             <p> Musique</p>
                             <p> Voyages<br/></p>
                             <p>Cuisine <br/></p>


                             </div>
                              <div class =\"sec-droite\">
                               <p><span class> pratique la guitare</span><br/></p>
                               <p><span class> Tunisie,Maroc ,Espagne,Almagne..</span><br/></p>
                               <p><span class> Lol</span><br/></p>


                         </div>
                        </div>

      </div>





{% endblock %}
", "home/cv.html.twig", "/home/nassimsd/Bureau/M2/TW/SYMPHONY/projetcv/templates/home/cv.html.twig");
    }
}
