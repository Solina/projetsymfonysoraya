<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerRlxe0rM\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerRlxe0rM/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerRlxe0rM.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerRlxe0rM\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerRlxe0rM\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'Rlxe0rM',
    'container.build_id' => '4e896479',
    'container.build_time' => 1575881907,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerRlxe0rM');
