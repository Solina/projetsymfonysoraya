<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Projet;
use App\Repository\ProjetRepository;

class HomeController extends AbstractController

{
/**
     * @Route("/home/projets", name="index")
     */
    
    public function index(ProjetRepository $repo)
    {//discuter avec doctrine
   // $repo=$this->getDoctrine()->getRepository(Projet::class);
    $projets=$repo->findAll();
   
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'projets' => $projets
        ]);
    }

    /**
    *@Route("/" , name="home")
    */

    public function home(){
    return $this->render('home/home.html.twig');
    }
     /**
        *@Route("/home/cv" , name="pagecv")
        */
     public function cv(){
        return $this->render('home/pagecv.html.twig');
        }
         /**
          *@Route("home/projet/{id}",name="projet")
          */
            public function show (ProjetRepository $repo,$id){
           
            $projet=$repo->find($id);
            return $this->render('home/projet.html.twig',['projet'=>$projet ]);
         }




}
