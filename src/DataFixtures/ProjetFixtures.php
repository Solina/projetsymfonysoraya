<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Projet;

class ProjetFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
  // for ($i=1;$i<=5;$i++){
        $projet =new Projet();
        $projet->setIntitule("Création d’un générateur de site web")
               ->setDescription("langages utilises javascript htm css")
               ->setLien("https://github.com/solinatoumi/generateur-de-sites-web")
               ->setCreatedAt(new \DateTime());
               $manager->persist($projet);

               $projet1 =new Projet();
        $projet1->setIntitule("projet MEAN")
               ->setDescription(" projet de vente en ligne")
               ->setLien("https://github.com/solinatoumi/symfoTest")
               ->setCreatedAt(new \DateTime());
               $manager->persist($projet1); 

               $projet2 =new Projet();
        $projet2->setIntitule("Projet Cv")
               ->setDescription("Framework Symfony")
               ->setLien("https://gitlab.com/Solina/projetsymfonysoraya")
               ->setCreatedAt(new \DateTime());
                $manager->persist($projet2); 

               $projet3 =new Projet();
        $projet3->setIntitule("Explorateur de fichiers")
               ->setDescription("Languages utilisés Python, JavaScript, CGI, HTML, CSS")
               ->setLien("https://github.com/solinatoumi/responsiveskeleton")
               ->setCreatedAt(new \DateTime());
               $manager->persist($projet3); 



               $projet4 =new Projet();
               $projet4->setIntitule("Adopter une these")
                      ->setDescription(" Création d'un site Offres de these au niveau des ecoles doctorales")
                      ->setLien("https://gitlab.com/Solina/adopterunethesetp3")
                      ->setCreatedAt(new \DateTime());
                      $manager->persist($projet4); 
        $manager->flush();
    }
}
