<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Asset\Tests\VersionStrategy;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Asset\VersionStrategy\StaticversionStrategy;

class StaticversionStrategyTest extends TestCase
{
    public function testGetVersion()
    {
        $version = 'v1';
        $path = 'test-path';
        $staticversionStrategy = new StaticversionStrategy($version);
        $this->assertEquals($version, $staticversionStrategy->getVersion($path));
    }

    /**
     * @dataProvider getConfigs
     */
    public function testApplyVersion($path, $version, $format)
    {
        $staticversionStrategy = new StaticversionStrategy($version, $format);
        $formatted = sprintf($format ?: '%s?%s', $path, $version);
        $this->assertEquals($formatted, $staticversionStrategy->applyVersion($path));
    }

    public function getConfigs()
    {
        return [
            ['test-path', 'v1', null],
            ['test-path', 'v2', '%s?test%s'],
        ];
    }
}
